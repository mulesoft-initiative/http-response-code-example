# **http-response-code-example**

## Summary
This mule code example will help you to understand and put in practice the REST HTTP responses. It will show you how to mapping and handle them with a simple example to evaluate and return the information of a product.  

*If you are not good familiar with the REST HTTP Responses, you can take a look at the following confluence page: [HTTP Response code](https://systemsandsoftwaresolutions.atlassian.net/wiki/spaces/IMP/pages/649297927/REST+Response+codes)*

---

## What contains this project
Below I list the multiple parts of this project

- The http-response-example.xml that contains the following four flows that are using a common HTTP Listener configuration placed in the global.xml file
   - get-product
   - get-products-2
   - valid-product
   - valid-product-2
- A dev properties file in which is already defined the requester and HTTP information like port and host.

> http.port=8081
> http.host=0.0.0.0
> requester.basepath=validate-product
> requester.host=localhost

- An arrangement of two postman requests to try the code, placed on the resource folder.

---
## Behavior of each flow
- **get-product-2 flow**

*Current state:* *stopped*

*Endpoint:* */get-product*

*Required query params:* *product*

*E.g.* **_localhost:8081/get-product?product=donitas_**

This is one of the main flows, sets a correlationId, and calls the validate-product flow consuming the endpoint */validate-product*. 
In the requester, I have configured the success HTTP response codes like 200, 404, and 420. As the 404 and  420 are categorized as a success response even if the validate-product flow throws a 404 or 420 instead of a 200 the flow will be completed successfully.

**Important:** The HTTP status code 404 means that the resource was not found, and the 420 is a code that is no in use for a certain reason, however, we can map codes and use them long in the API.    

---
- **get-product flow**

*Current state:* *started*

*Endpoint:* */get-product*

*Required query params:* *product*

*E.g.* **_localhost:8081/get-product?product=conchas_**

This is one of the main flows, sets a correlationId, and calls the validate-product flow consuming the endpoint */validate-product*. It is important to note, the request to validate-product flow is wrapped in a try scope, that will handle two types of exceptions **(HTTP:NOT FOUND and ANY)**.
In this case, I've set the requester up with the error mapping, so in case of catching one of the previous exceptions, I'm setting in the payload the following message: 'Requested product does not exist'.

**Important:** In the try scope, I'm handling the exception with an *On error continue* so if the transaction entered in the error handling side, the process will complete successfully either.  

---

- **validate-product**

*Current state:* *started*

*Endpoint:* */validate-product*

*Required query params:* *product*

*E.g.* **_localhost:8081/validate-product?product=roles_**

This is one of two flows in charge of 'validate' a product. After calling this endpoint, it verifies if the correlationId is populated or if it needs to assign a new once. Then, using an *is true validation module* verify if the product name equals to 'donitas. If the validation is false, then the module throws an exception that is being managed by the flow error handler. In this case, I used an *On Error Propagate* that will set a 420 status code and a reason phrase for it (Requested product does not exist). 

**Important:** In the HTTP Listener, I've configured the Error response to include on the Body the message: 'Requested product does not exist', the status code, and reason phrase out in the error handler section.

**Note:** The 420 error is not being recognized, so it will be categorized by an Unknown issue and it will be handled as ANY exception in get-product flows.

---
- **validate-product**

*Current state:* *started*

*Endpoint:* */validate-product*

*Required query params:* *product*

*E.g.* **_localhost:8081/validate-product?product=doraditas_**

This is one of two flows in charge of 'validate' a product. After calling this endpoint, it verifies if the correlationId is populated or if it needs to assign a new once. Then, using an *is true validation module* verify if the product name equals to 'donitas'. If the validation is false, then the module throws an exception that is being managed by the flow error handler. In this case, I used an *On Error Propagate* that will set a 404 status code and a reason phrase for it (Requested product does not exist). 

**Important:** In the HTTP Listener, I've configured the Error response to include on the Body the message: 'Requested product does not exist', the status code, and reason phrase out in the error handler section.

**Note:** The 404 will be captured in the exception handler as an HTTP:NOT FOUND in get-product flows.

---
- **set-correlation-subflow**

Subflow in the API in charge of set correlationId that will help as part of the audit trails and the Managed service team to track a certain transaction.
